#include "tracker.h"
//#include "construct_tracks.h"
#include <bitset>
#include <fstream>
#include <string>


bool check_hit(ap_int<64> in0,ap_int<64> in1){
	if ((in0.range(5, 10) == in1.range(5, 10)) && (in0.range(14, 11) == in1.range(14, 11)))
		return true;
	return false;
}

//bool f_sttHitCompareLeadTime ( SttRawHit a, SttRawHit b )
//{
//    return ( a.leadTime < b.leadTime );
//}

void track_bin_conc(ap_int<64> ina[HIT_LIM],ap_int<64> inb[HIT_LIM], ap_uint<11> ina_size, ap_uint<11> inb_size, l_Sfit_info &f_out){

#pragma HLS INTERFACE ap_fifo depth=64 port=f_out
#pragma HLS DATA_PACK variable=f_out
#pragma HLS INTERFACE ap_fifo depth=HIT_LIM port=inb
#pragma HLS INTERFACE ap_fifo depth=HIT_LIM port=ina




	ap_int<11> a_size = ina_size;
	ap_int<11> b_size = inb_size;
	int max_size = a_size < b_size ? b_size : a_size;
	const short event_size = a_size + b_size;


	SttRawHit data_in[HIT_LIM];
	ap_int<32> track[MAX_FT_TOTAL_LAYERS];
	short index=0;

	ap_int<64> ina_temp[HIT_LIM];
	ap_int<64> inb_temp[HIT_LIM];
//#pragma HLS array_partition variable=ina_temp
//#pragma HLS array_partition variable=inb_temp
//
//	for (int a = 0; a < max_size; a++) {
//#pragma HLS loop_tripcount min=26 max=26
//#pragma HLS pipeline
//		if (a < ina_size) {
//			ina_temp[a] = ina[a];
//		}
//		if (a < inb_size) {
//			inb_temp[a] = inb[a];
//		}
//	}

	short ind_a =0;
	short ind_b=0;
	a_loop :for(short a=0; a< a_size+b_size; a++){
		#pragma HLS pipeline
		a < a_size ? ina_temp[ind_a++] = ina[a] : inb_temp[ind_b++] = inb[a - a_size ];
	}


	ap_int<32> hit_array[MAX_FT_TOTAL_LAYERS+1];
	#pragma HLS array_partition variable=hit_array

	for(int a=0; a<MAX_FT_TOTAL_LAYERS+1; a++){
#pragma HLS unroll
		hit_array[a].range(31,0) =0;
	}
	short evt=0;
	//for(int a=0; a<HIT_LIM-1; a++){
	b_loop :for(short a=0; a<a_size-1; a++){
//	#pragma HLS loop_tripcount min=18 max=18
		if(ina_temp[a] !=0){
			if(check_hit(ina_temp[a],ina_temp[a+1])){
				SttRawHit  hit; //= new SttRawHit;
				short lay=ina_temp[a].range(14, 11);
				hit.straw = ina_temp[a].range(10, 5);
				hit.layer = ina_temp[a].range(14, 11);
//				hit.module = ina_temp[a].range(18, 15);
				hit.leadTime = ina_temp[a].range(50, 19);
//				hit.trailTime = ina_temp[a+1].range(50, 19);
				a++;
				data_in[index]=hit;
//				std::cout<<"A :"<<hit.layer<<"\t"<<hit.straw<<std::endl;
				hit_array[lay][hit.straw]= 1;
				index++;
			}
		}
	}
	evt = evt + index;

	//for(int a=0; a<HIT_LIM-1; a++){
	c_loop :for(short a=0; a<b_size-1; a++){
//	#pragma HLS loop_tripcount min=25 max=25
		if(inb_temp[a] !=0){
			if(check_hit(inb_temp[a],inb_temp[a+1])){
				SttRawHit  hit; //= new SttRawHit;
				short lay=inb_temp[a].range(14, 11);

				hit.straw = inb_temp[a].range(10, 5);
				hit.layer = inb_temp[a].range(14, 11);
//				hit.module = inb_temp[a].range(18, 15);
				hit.leadTime = inb_temp[a].range(50, 19);
//				hit.trailTime = inb_temp[a+1].range(50, 19);
				a++;
				data_in[index]=hit;
				hit_array[lay][hit.straw]= 1;
//				std::cout<<"B :"<<hit.layer<<"\t"<<hit.straw<<std::endl;
				index++;
			}
		}
	}
	evt = evt + index;
	std::cout<<event_size<<"\t"<<evt<<std::endl;


//	for(int h=0; h< MAX_FT_TOTAL_LAYERS+1; h++){
//		std::cout<<"hit :"<<std::bitset<32>(hit_array[h])<<std::endl;
//	}




//std::cout<<event_size<<"\t"<<HIT_LIM<<std::endl;
	//if (event_size < HIT_LIM){
	std::cout<<std::endl<<std::endl;


	fit_info f_info = construct_track(track,hit_array);
	float slope_out = 0,const_out=0;

	if((f_info.constant >0 || f_info.constant <0 ) && (f_info.slope > 0 || f_info.slope < 0)){
		std::cout<<"Track is in the form y[i]= "<<f_info.slope<<"* x[i] + "<<f_info.constant<<std::endl<<"Track candidates :\n";
//		std::cout<<"Layer\t"<<"Straw\t"<<"LT\t"<<"TT"<<std::endl;
//		for(short i=0; i<MAX_FT_TOTAL_LAYERS*2; i++){
//			std::cout<<track[i].layer<<"\t"<<track[i].straw<<"\t"<<track[i].leadTime<<std::endl;
//		}

		f_info.slope < 0 ? slope_out = (f_info.slope *-10000):slope_out =(f_info.slope *10000);
		f_info.constant < 0 ? const_out = (f_info.constant*-100):const_out =(f_info.constant*100);

		f_info.slope < 0 ? f_out.slope_sign=0:f_out.slope_sign=1;
		f_info.constant < 0 ? f_out.constant_sign =0:f_out.constant_sign=1;
		f_out.slope = (ap_int<31>) slope_out & 0x7FFFFFFF;
		f_out.constant = (ap_int<31>) const_out & 0x7FFFFFFF;
//		std::printf("Slope_sign %d, Slope %d , Const_sign %d Cnst %d \n",std::bitset<1>(f_out.slope_sign),std::bitset<31>(f_out.slope),std::bitset<1>(f_out.constant_sign),std::bitset<31>(f_out.constant));

	}

//}

}
