#include "construct_tracks.h"
#include "math.h"

/*MAX_PAIRS - MAX_COMB
 * 16 - 256,17 - 384, 18-575, 19-864, 20-1296, 21-1944, 22-2916, 23-4374, 24-6561
*/


//const int MAX_IN_LAY =10, PAIR =2,MAX_PAIRS_IN_LAY = 9, MAX_PAIRS=17,MAX_COMB=385;
//const int MAX_IN_LAY =10, PAIR =2,MAX_PAIRS_IN_LAY = 9, MAX_PAIRS=19,MAX_COMB=1298;

const int MAX_IN_LAY =10, PAIR =2,MAX_PAIRS_IN_LAY = 3, MAX_PAIRS=20, MAX_COMB=1296;

//max hits in a layer, max hits in a pair ;) ,max pairs in a layer, maximum possible track combinations

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
short numberOfSetBits(uint32_t i)
{
     i = i - ((i >> 1) & 0x55555555);
     i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
     return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}
//short numberOfSetBits(ap_int<32> i)
//{
//	short n=0;
//	for(short a=0; a< 31; a++){
//		if ((i[a]&1) == 1) n++;
//	}
//	return n;
//}

bool GP_b(ap_int<32> in_hit_array[MAX_FT_TOTAL_LAYERS+1],ap_int<32> pairs_hit_array[MAX_FT_TOTAL_LAYERS+1]){
	pairs_hit_array[0].range(31,0)=0;
//	short ret=0;
	for(short a=1; a< MAX_FT_TOTAL_LAYERS+1; a++){
#pragma HLS pipeline
		pairs_hit_array[a] = (in_hit_array[a] & (in_hit_array[a]<<1)) | (in_hit_array[a] & (in_hit_array[a]>>1)) ;
//		ret++;
		if(numberOfSetBits(pairs_hit_array[a]) <2)	return false;
	}
//	for (short b=1; b<MAX_FT_TOTAL_LAYERS+1; b++){
//		if(numberOfSetBits(pairs_hit_array[b]) <2)	return false;
//	}
	return true;//ret;
}

bool clusters_b(ap_int<32> pairs_hit_array[MAX_FT_TOTAL_LAYERS+1],ap_int<32> cluster_array[MAX_FT_TOTAL_LAYERS+1][MAX_PAIRS_IN_LAY],short no_pairs[MAX_FT_TOTAL_LAYERS]){
	for(short a=1; a< MAX_FT_TOTAL_LAYERS+1; a++){
//#pragma HLS pipeline
		ap_int<32> checker;
		checker.range(31,2)=0;
		checker[0]= 1;
		checker[1]= 1;
		short n=0,idx=0;
		for(short b=0; b<31;b++){
#pragma HLS pipeline
			n = numberOfSetBits(pairs_hit_array[a] & checker);
//			std::cout<<"No.of.pairs "<<idx<<std::endl;
			if (n ==2 && idx<MAX_PAIRS_IN_LAY)	{
				cluster_array[a][idx]=pairs_hit_array[a] & checker;
				idx++;
				no_pairs[a-1]++;
			}
			checker = checker <<1;
		}
	}
	return true;
}

//Get the layer index if more than 1 pair
short check_decri(short res[MAX_FT_TOTAL_LAYERS]){
	q_loop :for (short i=0; i<MAX_FT_TOTAL_LAYERS; i++){
#pragma HLS pipeline
        if (res[i] > 1){
            return i;
            break;
        }
    }
    return 8; //the value of the return here is not significant
}

//Get the combination of the pair indices, return no.of combinations
short index_comb(short res[MAX_FT_TOTAL_LAYERS],short arr_index[MAX_COMB][MAX_FT_TOTAL_LAYERS]){
	short original[MAX_FT_TOTAL_LAYERS];
	p_loop :for(short a=0; a< MAX_FT_TOTAL_LAYERS; a++){
#pragma HLS pipeline
        original[a]= res[a];
    }
    short first = check_decri(res);
    short no_comb=0;
    while(check_decri(res) != MAX_FT_TOTAL_LAYERS){
    	short current = check_decri(res);
    	r_loop :for(short i=0; i<MAX_FT_TOTAL_LAYERS; i++){
#pragma HLS pipeline
            arr_index[no_comb][i]=res[i];
        }
        res[check_decri(res)] -=1;
        no_comb++;
        if(first != current ){
        	s_loop :for(short j=0; j< current; j++){
#pragma HLS pipeline

                res[j]= original[j];
            }
        }
    }
    std::cout<<"no_comb "<<no_comb<<std::endl;
    return no_comb;
}

void get_straw_no(ap_int<32> track_b[MAX_FT_TOTAL_LAYERS],short track_straw[MAX_FT_TOTAL_LAYERS]){
	short idx=0;
	for(short a=0; a<MAX_FT_TOTAL_LAYERS; a++){
#pragma HLS pipeline
		if (a == 0 || a== 3 || a== 4 || a== 7){
			for(short b=0; b< 31; b++){
#pragma HLS pipeline
				if ((track_b[a][b] & 1) ==1){
					track_straw[idx++] = b;
				}
			}
		}

	}
}

fit_info get_chi2_b(short track_straw[MAX_FT_TOTAL_LAYERS]){
//#pragma HLS latency min=500 max=2500
	fit_info s;
	float sums[4];
	float XSqr =0,CriticalValue=0;
	sums[0]= 0; //xsum
	sums[1]= 0; //ysum
	sums[2]= 0;	//m=0 x2sum
	sums[3]= 0;	//c=0 xysum

	short 	hit_index[MAX_FT_TOTAL_LAYERS]	=	{1,2,7,8,9,10,15,16};

	for(short i=0; i < MAX_FT_TOTAL_LAYERS; i++){
			sums[0]	+=	hit_index[i];
			sums[1]	+=	track_straw[i];
			sums[2]	+=	(hit_index[i]*hit_index[i]);
			sums[3]	+=	(hit_index[i]*track_straw[i]);
//			std::cout<<sums[0]<<"\t"<<sums[1]<<"\t"<<sums[2]<<"\t"<<sums[3]<<"\t"<<std::endl;
	}

	s.slope = (MAX_FT_TOTAL_LAYERS * sums[3] - sums[0] * sums[1])/(MAX_FT_TOTAL_LAYERS * sums[2] - sums[0] * sums[0]);
	s.constant = (sums[2] * sums[1] - sums[0] * sums[3])/(sums[2] * MAX_FT_TOTAL_LAYERS - sums[0] * sums[0]);

//	std::cout<<"xsum:"<<sums[0]<<"\t ysum :"<<sums[1]<<"\t x2sum:"<<sums[2]<<"\txysum:"<<sums[3]<<std::endl;

	x_loop :for(short f=0; f<MAX_FT_TOTAL_LAYERS; f++){
#pragma HLS pipeline
			XSqr = s.slope * track_straw[f] + s.constant - track_straw[f];    	//Observed[I] - Expected[I]
			CriticalValue += ((XSqr * XSqr) / track_straw[f]);	//((XSqr * XSqr) / Expected[I])
	}
	s.chi2 = CriticalValue;

//	std::cout<<"chi2:"<<s.chi2<<"\t slope :"<<s.slope<<"\t constant:"<<s.constant<<std::endl;
	return s;
}

short smallest_chi(float chi2_track_comb[MAX_COMB], short n){
   float temp = chi2_track_comb[0];
   short ind=0;
   y_loop :for(short i=0; i<n; i++) {
#pragma HLS pipeline
        if(chi2_track_comb[i]>0.000){
            if(temp>chi2_track_comb[i]) {
                temp=chi2_track_comb[i];
                ind = i;
            }
        }
   }
   std::cout<<n<<"\t smallest chi indx"<<ind<<std::endl;
   return ind;
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


//*****************************************************************************************************************************

fit_info construct_track(ap_int<32> track_b[MAX_FT_TOTAL_LAYERS ],ap_int<32> hit_array[MAX_FT_TOTAL_LAYERS+1]){

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	ap_int<32> pairs_hit_array[MAX_FT_TOTAL_LAYERS+1];						//^^
	ap_int<32> cluster_array[MAX_FT_TOTAL_LAYERS+1][MAX_PAIRS_IN_LAY];		//^^
	short no_pairs[MAX_FT_TOTAL_LAYERS]={0,0,0,0,0,0,0,0};					//^^
	short index_array[MAX_COMB][MAX_FT_TOTAL_LAYERS];						//^^
	short track_straw[MAX_FT_TOTAL_LAYERS];									//^^
	fit_info f_info;														//^^
	ap_int<32> sum_arr;														//^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#pragma HLS array_partition variable=pairs_hit_array
#pragma HLS array_partition variable=no_pairs
//#pragma HLS RESOURCE variable=cluster_array core=RAM_2P_BRAM
bool tracks_found = false;

	for(short c=0; c<MAX_FT_TOTAL_LAYERS+1; c++){
#pragma HLS pipeline
		for(short cc=0; cc<MAX_PAIRS_IN_LAY; cc++){
#pragma HLS pipeline
			cluster_array[c][cc].range(31,0)=0;
		}
		if (c <MAX_FT_TOTAL_LAYERS)
			track_b[c].range(31,1)=0;
	}
	for(short a=0; a< MAX_COMB; a++){
#pragma HLS pipeline
		for(short b=0; b< MAX_FT_TOTAL_LAYERS; b++){
#pragma HLS pipeline
			index_array[a][b]=0;
		}
	}

	if(GP_b(hit_array,pairs_hit_array) /*&& numberOfSetBits(sum_arr)>3*/){
		float 		chi2_track_comb[MAX_COMB];
		clusters_b(pairs_hit_array,cluster_array,no_pairs);
		short pair_sum=0;
		for(short a=0; a< MAX_FT_TOTAL_LAYERS; a++){
#pragma HLS pipeline
			pair_sum += no_pairs[a];
		}
		std::cout<<"pair_sum "<<pair_sum<<"\t"<<MAX_PAIRS<<std::endl;
		if (pair_sum <= MAX_PAIRS){
			short no_of_comb_b = index_comb(no_pairs,index_array);
			for(int i=0; i<MAX_FT_TOTAL_LAYERS; i++){
#pragma HLS pipeline
				index_array[no_of_comb_b][i]=no_pairs[i];
			}
			std::cout<<no_of_comb_b<<std::endl;
			if (no_of_comb_b<1){
				for(short a =1; a< MAX_FT_TOTAL_LAYERS+1; a++){
#pragma HLS pipeline
					track_b[a-1] = cluster_array[a][0];
//					std::cout<<std::bitset<32>(cluster_array[a][index_array[0][a-1]-1])<<"\n";
				}
//				std::cout<<std::endl;
				tracks_found = true;
			}
			else {
				for(short j=0; j<no_of_comb_b+1; j++){
					short idx1=0;
					for(short a=1; a < MAX_FT_TOTAL_LAYERS+1; a++){
#pragma HLS pipeline
						track_b[a-1] = cluster_array[a][index_array[j][a-1]-1];
					}
					get_straw_no(track_b,track_straw);
					chi2_track_comb[j]=get_chi2_b(track_straw).chi2;
				}
				short combination_index = smallest_chi(chi2_track_comb,no_of_comb_b+1); //get the combination index of the track with the lowest chi2;
				for(short a=1; a < MAX_FT_TOTAL_LAYERS+1; a++){
#pragma HLS pipeline
					track_b[a-1] = cluster_array[a][index_array[combination_index][a-1]-1];
				}
				tracks_found = true;
			}
			get_straw_no(track_b,track_straw);
			f_info = get_chi2_b(track_straw);
//			std::cout<<"chi2:"<<f_info.chi2<<"\t slope :"<<f_info.slope<<"\t constant:"<<f_info.constant<<std::endl;
	    }

	}
	if (tracks_found == false){
		f_info.chi2 = 0;
		f_info.constant = 0;
		f_info.slope = 0;
	}
	return f_info;
}
