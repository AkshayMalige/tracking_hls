//#include <cstdint>
#ifndef H_STT_RAW_HIT
#define H_STT_RAW_HIT

#include <ap_int.h>


class SttRawHit
{
  public:
//    int channel; // 0-49
//    uint32_t tdcid;
//    ap_uint<32> tdcid;
//    int stt_tdc_event_sizes;
    double leadTime;
    double trailTime;
//    double tot;
//    bool isRef;

//    double drifttime;
//    short station;
//    short plane;
    short layer;
    short module;
//    short cell;
    short straw;
//    double x;
//    double y;
//    double z;

    SttRawHit() {
    	leadTime = 0;
    	trailTime = 0;
    	layer = 0;
    	module = 0;
    	straw = 0;
    }

   // virtual ~SttRawHit() {}

//    void SetChannel(int c) { channel = c; }

//    ClassDef(SttRawHit, 1)
};
#endif
