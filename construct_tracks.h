#ifndef CONSTRUCT_TRACK_H
#define CONSTRUCT_TRACK_H
#include<iostream>
#include <ap_int.h>
#include <fstream>
#include <string>
#include <bitset>
#include <stdint.h>

//#include "SttRawHit.h"

const int MAX_FT_TOTAL_LAYERS = 8;
const int TIME_BINS = 256;
//const int TIME_BINS = 16;
const int bins = TIME_BINS / 2 - 2 + 1 ;
const int LINKS = 2;
const int HIT_LIM = 75;

struct fit_info{
	float chi2;
	float slope;
	float constant;
};

typedef struct SttRawHit
{
	float leadTime=0;
//    double trailTime=0;
    short layer=0;
//    short module=0;
    short straw=0;
}l_SttRawHit;

fit_info construct_track(ap_int<32> track[MAX_FT_TOTAL_LAYERS],ap_int<32> hit_array[MAX_FT_TOTAL_LAYERS+1]);

#endif
