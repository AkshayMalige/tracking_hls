#include "construct_tracks.h"

#include<iostream>
#include <ap_int.h>
#include <fstream>
#include <string>
#include <bitset>

typedef struct Sfit_info
{
	ap_int<1> slope_sign;
	ap_int<31> slope;
	ap_int<1> constant_sign;
	ap_int<31> constant;

} l_Sfit_info;


void track_bin_conc(ap_int<64> ina[HIT_LIM],ap_int<64> inb[HIT_LIM], ap_uint<11> ina_size, ap_uint<11> inb_size, l_Sfit_info &f_out);
