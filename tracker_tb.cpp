#include <iostream>
#include "tracker.h"
#include <ap_int.h>
#include <fstream>
#include <string>
//#include "SttRawHit.h"
#include <bitset>


using namespace std;


void data_clear(ap_int<64> data[LINKS][bins][HIT_LIM]){
	for (int l=0; l<LINKS; l++){
		for (int i=0; i <bins; i++){
			//memset(data[l][i],0,sizeof(HIT_LIM));
			for(int j=0; j< HIT_LIM; j++){
				data[l][i][j]=0;
			}
		}
	}
}


int main(){

	ap_int<64> data[LINKS][bins][HIT_LIM];
	std::string line;
//	std::ifstream time_file0("/home/amalige/Filtering/concentrator_tracking/concentrator/work/concentrator/concentrator.sim/sim_1/behav/xsim/time_bin_link0.txt");
//	std::ifstream time_file1("/home/amalige/Filtering/concentrator_tracking/concentrator/work/concentrator/concentrator.sim/sim_1/behav/xsim/time_bin_link1.txt");
	std::ifstream time_file0("/home/amalige/Filtering/concentrator_tracking/concentrator/work/concentrator/concentrator.sim/sim_1/behav/xsim/16time_bin_data0.txt");
	std::ifstream time_file1("/home/amalige/Filtering/concentrator_tracking/concentrator/work/concentrator/concentrator.sim/sim_1/behav/xsim/16time_bin_data1.txt");

	ofstream coutfile;
	coutfile.open("out.txt");

	if((!time_file0) && (!time_file1)){
		std::cerr<<"Can not open file ! \n";
	}
	else {
		std::cout<<"File openend ! \n";
	}


	bool bin_found = false;
	int time_bin =0;

	bool read0 = true;
	bool read1 = false;
	int evnt_n0 = 0;

	int bin_index0[bins];
	int bin_index1[bins];

	int arr_bin[bins];

	ap_int<bins> valid_time_bins0;
	ap_int<bins> valid_time_bins1;
	ap_int<bins> valid_time_bins;
	ap_int<bins+1> buf_time_bins;
	ap_int<bins> buf_time_bins_out;

	memset(bin_index0,0,sizeof(bins));
	memset(bin_index1,0,sizeof(bins));

	while (!time_file0.eof() && !time_file1.eof()){
		std::cout<<"************************** Event : "<<evnt_n0<<" **************************"<<std::endl;

		while (read0 == true && read1 == false){
		getline(time_file0,line);
				if (line.length() >0 && line == "new_fetch"){

					//std::cout <<"Event No: "<< evnt_n0 <<"\n"<<line<<"  FILE 0\n\n";
					//memset(bin_index0,0,sizeof(bins));
					for(int m=0; m<bins; m++){
						bin_index0[m] = 0;
						arr_bin[m] = 0;
						//std::cout<<m<<","<<bin_index0[m]<<"\t";
					}

					//std::cout<<"new_fetch0"<<std::endl;
					data_clear(data);
					read0 = true;
					valid_time_bins0.range(bins-1, 0) = 0;
					valid_time_bins1.range(bins-1, 0) = 0;
					buf_time_bins.range(bins, 0) = 0;


				}
				else if (line.length() >0 && line == "end_data"){

					//std::cout << line<<"\n\n";
					//std::cout<<"end_data0"<<std::endl;
					read0 = false;
					read1 = true;
				}
				else if (line.length() == bins){

					valid_time_bins0.range(112, 126)   	= std::stoi(line.substr(112, 15), 0, 2);
					valid_time_bins0.range(96, 	111)    = std::stoi(line.substr(96,  16), 0, 2);
					valid_time_bins0.range(80, 	95)  	= std::stoi(line.substr(80,  16), 0, 2);
					valid_time_bins0.range(64, 	79)  	= std::stoi(line.substr(64,  16), 0, 2);
					valid_time_bins0.range(48, 	63)  	= std::stoi(line.substr(48,  16), 0, 2);
					valid_time_bins0.range(32, 	47)  	= std::stoi(line.substr(32,  16), 0, 2);
					valid_time_bins0.range(16, 	31) 	= std::stoi(line.substr(16,  16), 0, 2);
					valid_time_bins0.range(0, 	15)		= std::stoi(line.substr(0,   16), 0, 2);

					//std::cout <<"a "<<line<<"\n";
//					std::cout<<"b "<<std::bitset<32>(valid_time_bins0.range(0, 31))<<std::bitset<32>(valid_time_bins0.range(32, 63))<<std::bitset<32>(valid_time_bins0.range(64, 95))<<std::bitset<31>(valid_time_bins0.range(96, 126))<<std::endl;


				}
				else{
						if (line.length() > 0 && line.length()<64){

							bin_found = true;
							read0 = true;
							time_bin = std::stoi(line);
							bin_index0[time_bin]++;
							//std::cout<<" Bin No  :  "<<time_bin<<"\n";
						//	buf_time_bins.range(time_bin,time_bin+1) = 1;
							int bit_slot = time_bin / 32;
							int bit = time_bin % 32 +1;
							//buf_time_bins.range(32 * bit_slot ,32 * bit_slot+31) = buf_time_bins.range(32 * bit_slot ,32 * bit_slot+31) | 1 <<  bit;
							//buf_time_bins = buf_time_bins | 1 << time_bin;
							arr_bin[time_bin]=1;
							if (bit < 32){
								buf_time_bins.range(0,31) = buf_time_bins.range(0,31) | 1 <<  bit;
							}
							//std::cout<<"timebin "<<time_bin<<"  "<<time_bin % 32<<" bit"<<bit<<endl;


						}
						else if (line.length() == 64 && bin_found == true){
							//std::cout << "Data : "<<line<<"\t"<<time_bin<<"\t"<<bin_index0[time_bin]<<"\n";
							bin_found = false;
							read0 = true;
							//std::cout<<"data index "<<time_bin<<"\t"<<bin_index0[time_bin]-1<<std::endl;

							ap_int<64> a;
							a.range(15, 0)  = (unsigned)std::stoi(line.substr(48,  16), 0, 2);
							a.range(31, 16) = (unsigned)std::stoi(line.substr(32,  16), 0, 2);
							a.range(47, 32) = (unsigned)std::stoi(line.substr(16,  16), 0, 2);
							a.range(63, 48) = (unsigned)std::stoi(line.substr(0,  16), 0, 2);

//							a.range(48, 63)  = (unsigned)std::stoi(line.substr(48,  16), 0, 2);
//							a.range(32, 47) = (unsigned)std::stoi(line.substr(32,  16), 0, 2);
//							a.range(16, 31) = (unsigned)std::stoi(line.substr(16,  16), 0, 2);
//							a.range(0, 15) = (unsigned)std::stoi(line.substr( 0,  16), 0, 2);

							data[0][time_bin][bin_index0[time_bin] - 1] = a;

						}
				}

			}

		while (read1 == true && read0 == false){
		getline(time_file1,line);
				if (line.length() >0 && line == "new_fetch"){

					//std::cout << line<<"  FILE 1\n\n";
					//memset(bin_index1,0,sizeof(bins));
					for(int m=0; m<bins; m++){
						bin_index1[m] = 0;
						//std::cout<<m<<","<<bin_index1[m]<<"\t";
					}
					//std::cout<<"new_fetch1"<<std::endl;
					//data_clear(data);
					read1 = true;

					evnt_n0++;
				}
				else if (line.length() >0 && line == "end_data"){

					//std::cout << line<<"\n\n";
					//std::cout<<"end_data1"<<std::endl;
					read1 = false;
					read0 = true;
				}
				else if (line.length() == bins){

					valid_time_bins1.range(112, 126)   	= std::stoi(line.substr(112, 15), 0, 2);
					valid_time_bins1.range(96, 	111)    = std::stoi(line.substr(96,  16), 0, 2);
					valid_time_bins1.range(80, 	95)  	= std::stoi(line.substr(80,  16), 0, 2);
					valid_time_bins1.range(64, 	79)  	= std::stoi(line.substr(64,  16), 0, 2);
					valid_time_bins1.range(48, 	63)  	= std::stoi(line.substr(48,  16), 0, 2);
					valid_time_bins1.range(32, 	47)  	= std::stoi(line.substr(32,  16), 0, 2);
					valid_time_bins1.range(16, 	31) 	= std::stoi(line.substr(16,  16), 0, 2);
					valid_time_bins1.range(0, 	15)		= std::stoi(line.substr(0,   16), 0, 2);

				//	std::cout << valid_time_bins<<"\n";

				}
				else{
						if (line.length() > 0 && line.length()<64){
							//std::cout<<" Bin No  :  "<<line<<"\n";
							bin_found = true;
							read1 = true;
							time_bin = std::stoi(line);
							bin_index1[time_bin]++;
						//	buf_time_bins.range(time_bin,time_bin+1) = 1;
							int bit_slot = time_bin / 32;
							int bit = time_bin % 32 +1;

							arr_bin[time_bin]=1;
							//buf_time_bins = buf_time_bins | 1 << time_bin;
//							buf_time_bins.range(32 * bit_slot ,32 * bit_slot+31) = buf_time_bins.range(32 * bit_slot ,32 * bit_slot+31) | 1 <<  bit;
							//buf_time_bins.range(32 * bit_slot ,32 * bit_slot+31) = buf_time_bins.range(32 * bit_slot ,32 * bit_slot+31) | 1 <<  bit;


						}
						else if (line.length() == 64 && bin_found == true){
							//std::cout << "Data : "<<line<<"\t"<<time_bin<<"\t"<<bin_index1[time_bin]<<"\n";

							ap_int<64> a;
							a.range(15, 0)  = (unsigned)std::stoi(line.substr(48,  16), 0, 2);
							a.range(31, 16) = (unsigned)std::stoi(line.substr(32,  16), 0, 2);
							a.range(47, 32) = (unsigned)std::stoi(line.substr(16,  16), 0, 2);
							a.range(63, 48) = (unsigned)std::stoi(line.substr( 0,  16), 0, 2);

//							a.range(48, 63)  = (unsigned)std::stoi(line.substr(48,  16), 0, 2);
//							a.range(32, 47) = (unsigned)std::stoi(line.substr(32,  16), 0, 2);
//							a.range(16, 31) = (unsigned)std::stoi(line.substr(16,  16), 0, 2);
//							a.range(0, 15) = (unsigned)std::stoi(line.substr( 0,  16), 0, 2);

							bin_found = false;
							read1 = true;
							data[1][time_bin][bin_index1[time_bin] - 1] = a;

						}
				}

			}


		//data_print(data);
		ap_int<64> data_in0[HIT_LIM];
		ap_int<64> data_in1[HIT_LIM];
		l_Sfit_info data_out;
		std::ostringstream os;


		if(valid_time_bins0 == valid_time_bins1)
		{
			valid_time_bins = valid_time_bins0;
			//std::cout<<valid_time_bins<<endl;
		}
		else {valid_time_bins.range(bins-1, 0) = 0;}

		//std::cout<<"b ";
		for(int bi=0; bi<bins+1; bi++){
			//std::cout<<arr_bin[bi];
			os << arr_bin[bi];
		}
		//std::cout<<endl;
		std::string str(os.str());
		//std::cout<<"c "<<str<<endl;

		buf_time_bins.range(112, 	127)   	= std::stoi(str.substr(112, 16), 0, 2);
		buf_time_bins.range(96,  	111)    = std::stoi(str.substr(96,  16), 0, 2);
		buf_time_bins.range(80,   	95)  	= std::stoi(str.substr(80,  16), 0, 2);
		buf_time_bins.range(64,   	79)  	= std::stoi(str.substr(64,  16), 0, 2);
		buf_time_bins.range(48,   	63)  	= std::stoi(str.substr(48,  16), 0, 2);
		buf_time_bins.range(32, 	47)  	= std::stoi(str.substr(32,  16), 0, 2);
		buf_time_bins.range(16, 	31) 	= std::stoi(str.substr(16,  16), 0, 2);
		buf_time_bins.range(0, 		15)		= std::stoi(str.substr(0,   16), 0, 2);

		buf_time_bins_out(0, 	126) = buf_time_bins.range(0, 	126);

		//std::cout<<"d "<<std::bitset<32>(buf_time_bins.range(0, 31))<<std::bitset<32>(buf_time_bins.range(32, 63))<<std::bitset<32>(buf_time_bins.range(64, 95))<<std::bitset<32>(buf_time_bins.range(96, 127))<<std::endl;
		//std::cout<<"a "<<std::bitset<32>(valid_time_bins.range(0, 31))<<std::bitset<32>(valid_time_bins.range(32, 63))<<std::bitset<32>(valid_time_bins.range(64, 95))<<std::bitset<31>(valid_time_bins.range(96, 126))<<std::endl;
		//std::cout<<"b "<<std::bitset<32>(buf_time_bins_out.range(0, 31))<<std::bitset<32>(buf_time_bins_out.range(32, 63))<<std::bitset<32>(buf_time_bins_out.range(64, 95))<<std::bitset<31>(buf_time_bins_out.range(96, 126))<<std::endl;
		//std::cout<<endl;

		for(int b=0; b<bins; b++){

//			ap_int<8> active_fifo_index;

			for(int h=0; h<HIT_LIM; h++){
				data_in0[h] = data[0][b][h];
				data_in1[h] = data[1][b][h];
			}
//			active_fifo_index.range(7,0) = std::stoi(std::to_string(b));
//			std::cout<<std::bitset<8>(active_fifo_index.range(7, 0))<<endl;

			std::cout<<"*************************  bin no: "<<b<<"  *************************  "<<std::endl;

			if (valid_time_bins[b] == 1 ){
				track_bin_conc(data_in0,data_in1, HIT_LIM, HIT_LIM, data_out);
//				std::cout<<"DONE"<<b<<std::endl;
			}
		}


//		for(int h=0; h<HIT_LIM; h++){
//			data_in0[h] = data[0][53][h];
//			data_in1[h] = data[1][53][h];
//		}
//
//		track_bin_conc(data_in0,data_in1, 19, 26, data_out);
//		std::cout<<"DONE"<<53<<std::endl;


		return 0;

	}
	time_file0.close();
	time_file1.close();

	return 0;

}


